import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

const styles = () => ({
  tooltip: {
    backgroundColor: '#616161',
  },
});

export default withStyles(styles)(({ children, classes, ...props }) => (
  <Tooltip classes={{ popper: classes.popper, tooltip: classes.tooltip }} {...props}>
    {children}
  </Tooltip>
));
