import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Globe from '@material-ui/icons/Public';
import {
  Bar,
  Row,
  Scatter,
  HSymbol,
  VSymbol,
  Timeline,
  StackedBar,
} from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage } from '../../../i18n';
import { Button, Menu } from './helpers';
import { mapOptions } from '../../../lib/settings';

const charts = [
  {
    Icon: Bar,
    id: 'BarChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.bar" />,
  },
  {
    Icon: Row,
    id: 'RowChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.row" />,
  },
  {
    Icon: Scatter,
    id: 'ScatterChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.scatter" />,
  },
  {
    Icon: HSymbol,
    id: 'HorizontalSymbolChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.horizontalsymbol" />,
  },
  {
    Icon: VSymbol,
    id: 'VerticalSymbolChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.verticalsymbol" />,
  },
  {
    Icon: Timeline,
    id: 'TimelineChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.timeline" />,
  },
  {
    Icon: StackedBar,
    id: 'StackedBarChart',
    message: <FormattedMessage id="de.visualisation.toolbar.chart.stacked" />,
  },
];

const useStyles = makeStyles(theme => ({
  icon: {
    minWidth: theme.spacing(4),
  },
}));

const Component = ({ chart, changeChart, selected, hasRefAreaDimension }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<AssessmentOutlinedIcon />}
        selected={R.or(selected, Boolean(anchorEl))}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.chart" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(({ id, Icon, message }) => (
          <MenuItem
            key={id}
            onClick={itemClick(changeChart, id)}
            aria-pressed={R.equals(chart, id)}
            dense
            selected={R.equals(chart, id)}
            id={id}
          >
            <ListItemIcon className={classes.icon}>
              <Icon fontSize="small" color="primary" />
            </ListItemIcon>
            <ListItemText id={id} primaryTypographyProps={{ color: 'primary' }}>
              {message}
            </ListItemText>
          </MenuItem>
        ))(charts)}
        {hasRefAreaDimension &&
          R.map(
            ({ id, mapId, levelId }) => (
              <MenuItem
                key={id}
                onClick={itemClick(changeChart, id, { map: { mapId, levelId } })}
                aria-selected={R.equals(chart, id)}
                dense
              >
                <ListItemIcon className={classes.icon}>
                  <Globe fontSize="small" color="primary" />
                </ListItemIcon>
                <ListItemText primaryTypographyProps={{ color: 'primary' }}>
                  <FormattedMessage
                    id="chart.choropleth"
                    values={{ map: `map.${mapId}.${levelId}` }}
                  />
                </ListItemText>
              </MenuItem>
            ),
            mapOptions,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  changeChart: PropTypes.func.isRequired,
  chart: PropTypes.string,
  selected: PropTypes.bool,
  hasRefAreaDimension: PropTypes.bool,
};

export default Component;
