import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getLocale, getIsRtl } from '../selectors/router';
import { changeLocale } from '../ducks/router';

export default connect(
  createStructuredSelector({
    localeId: getLocale,
    isRtl: getIsRtl,
  }),
  { changeLocale },
);
