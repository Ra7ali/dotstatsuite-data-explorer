import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  appBar: {
    minWidth: 300,
  },
  toolBar: {
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  logoWrapper: {
    flexGrow: 1,
  },
  logo: {
    maxHeight: 45,
    width: 'auto',
  },
  backLink: {
    marginBottom: theme.spacing(2),
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  backIcon: {
    fontSize: '0.75rem',
    marginBottom: -1
  },
}));
