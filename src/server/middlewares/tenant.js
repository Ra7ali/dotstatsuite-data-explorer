const tenant = configProvider => (req, res, next) => {
  const tenantId = req.query.tenant || req.headers['x-tenant'] || 'default';
  configProvider
    .getTenant(tenantId)
    .then(tenant => {
      req.tenant = tenant;
      next();
    })
    .catch(next);
};

module.exports = tenant;
