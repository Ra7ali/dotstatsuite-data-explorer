import React from 'react';
import PropTypes from 'prop-types';
import { ExpansionPanel } from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage } from '../../i18n';
import { PANEL_NARROW_FILTER } from '../../utils/constants';

const NarrowFilters = ({ isNarrow, children }) => {
  if (isNarrow) {
    return (
      <ExpansionPanel
        id={PANEL_NARROW_FILTER}
        label={<FormattedMessage id="de.side.filters.action" />}
        maxHeight={false}
      >
        {children}
      </ExpansionPanel>
    );
  }
  return children;
};

NarrowFilters.protoTypes = {
  isNarrow: PropTypes.bool,
  children: PropTypes.node,
};

export default NarrowFilters;
