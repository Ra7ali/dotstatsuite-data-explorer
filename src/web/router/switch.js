import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as R from 'ramda';
import { getLocation } from '../selectors/router';

const Switch = ({ location, routes = {} }) => {
  const pathname = R.prop('pathname', location);
  const Route = R.prop(pathname)(routes);
  if (R.isNil(Route)) return <div>notFound ({pathname})</div>;
  return <Route />;
};

Switch.propTypes = {
  location: PropTypes.object,
  routes: PropTypes.object,
};

export default connect(createStructuredSelector({ location: getLocation }))(Switch);
