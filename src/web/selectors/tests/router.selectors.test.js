import * as router from '../router';

jest.mock('../../lib/settings', () => ({ sdmxPeriod: [4, 5] }));

describe('selectors', () => {
  describe('router', () => {
    it('should get viewer', () => {
      expect(router.getViewer({ router: { params: { viewer: 'star' } } })).toEqual('star');
    });
    it('default viewer should be table', () => {
      expect(router.getViewer({ router: { params: {} } })).toEqual('table');
    });
  });
});
