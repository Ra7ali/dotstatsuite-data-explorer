const R = require('ramda');
const debug = require('debug');
const evtX = require('evtx').default;
const initHealthcheck = require('./healthcheck');

const loginfo = debug('webapp:evtx');

const services = [initHealthcheck];

const initServices = evtx => R.reduce((acc, service) => acc.configure(service), evtx, services);

const init = ctx => {
  const {
    config: { gitHash },
    configProvider,
  } = ctx;
  const globals = { configProvider, startTime: new Date(), gitHash };
  const healthcheck = evtX(globals).configure(initServices);
  loginfo('monitoring service up.');
  return Promise.resolve({
    ...ctx,
    services: { ...ctx.services, healthcheck },
  });
};

module.exports = init;
