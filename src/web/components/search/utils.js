import * as R from 'ramda';
import { searchConstants } from '../../lib/search';
import { formatMessage } from '../../i18n';
import { defineMessages } from 'react-intl';

const messages = defineMessages({
  datasourceId: { id: 'de.search.datasourceId' },
});

const getStaticMessage = intl =>
  R.cond([
    [R.equals(searchConstants.DATASOURCE_ID), R.always(formatMessage(intl)(messages.datasourceId))],
    [R.T, R.identity],
  ]);

export const getLabel = ({ intl }) => ({ id, label }) =>
  R.isNil(label) ? getStaticMessage(intl)(id) : label;
