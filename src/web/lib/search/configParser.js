import * as R from 'ramda';
import bs62 from 'bs62';
import C from './constants';
import facetsParser from './facetsParser';

export default parserArgs => data => {
  const configuredFacetsParser = facetsParser({ config: R.propOr({}, 'config', parserArgs) });

  if (R.isEmpty(R.propOr([], 'facetIds', parserArgs)))
    return { locale: data.locale, facets: configuredFacetsParser(data.facets) };

  const encodedIds = R.map(
    R.ifElse(R.equals(C.DATASOURCE_ID), R.identity, bs62.encode),
    parserArgs.facetIds,
  );
  const homeFacets = R.pick(encodedIds, data.facets);

  const parsedHomeFacets = configuredFacetsParser(homeFacets);

  return { locale: data.locale, facets: parsedHomeFacets };
};
