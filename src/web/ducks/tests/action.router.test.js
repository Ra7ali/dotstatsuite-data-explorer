import {
  CHANGE_TERM,
  CHANGE_CONSTRAINTS,
  CHANGE_FACET,
  CHANGE_START,
  changeTerm,
  changeConstraints,
  changeFacet,
  changeStart,
} from '../router';

jest.mock('../../lib/settings', () => ({ getLocaleId: v => v, theme: { visFont: '' } }));

describe('router actions', () => {
  it('should handle action changeTerm', () => {
    const action = {
      type: CHANGE_TERM,
      payload: { term: 'toto' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeTerm({ term: 'toto' })).toEqual(action);
  });

  it('should handle action changeConstraints', () => {
    const action = {
      type: CHANGE_CONSTRAINTS,
      payload: { facetId: 'titi', constraintId: 'toto' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeConstraints('titi', 'toto')).toEqual(action);
  });

  it('should handle action changeFacet', () => {
    const action = {
      type: CHANGE_FACET,
      payload: { facetId: 'titi' },
      pushHistory: '/',
    };
    expect(changeFacet('titi')).toEqual(action);
  });

  it('should handle action changeStart', () => {
    const action = {
      type: CHANGE_START,
      payload: { start: 'tutu' },
      pushHistory: '/',
      request: 'getSearch',
    };
    expect(changeStart('tutu')).toEqual(action);
  });
});
